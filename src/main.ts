import { createApp } from 'vue'
import Home from './Home.vue'
import router from './router'
import store from './store'
import pluginAntd from './plugins/antd'
import VueDND from 'awe-dnd'

createApp(Home).use(store)
  .use(router)
  .use(VueDND)
  .use(pluginAntd)
  .mount('#app')
