import { App } from '@vue/runtime-core'
import { ConfigProvider, Button, DatePicker, message } from 'ant-design-vue'

export default {
  install: function (app: App): void {
    // Plugin code goes here
    app.use(Button)
    app.use(DatePicker)
    app.use(ConfigProvider)
    app.config.globalProperties.$message = message
  }
}
