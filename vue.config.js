'use strict'

const os = require('os')

const IS_PROD = ['production', 'prod'].includes(process.env.NODE_ENV)

module.exports = {
  productionSourceMap: !IS_PROD,
  parallel: os.cpus().length > 1,
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            'primary-color': '#1DA57A',
            'link-color': '#1DA57A',
            'border-radius-base': '2px'
          },
          javascriptEnabled: true
        }
      }
    }
  }
}
